# Tuesday Morning Challenge

1. Create a playbook that does the following:
- targets the host group called `planetexpress`
- runs a raw module task to ensure python3-pip is installed

2. Improve your playbook:
- runs a task to apt install two packages - figlet & hollywood


